# -----------------------------------------------------------------------------
# include libraries and set defaults
# -----------------------------------------------------------------------------
import csv
import operator
import re
from datetime import datetime
from os import mkdir

import serial.tools.list_ports
import serial
import configparser

# -----------------------------------------------------------------------------
# global variables
# -----------------------------------------------------------------------------

global selected_port
global serial_baud_rate

global uart
global file_name
global json_data

serial_timeout_read = 5
folder_output = "csv"


def get_configfile_info():
    global selected_port
    global serial_baud_rate
    global json_data
    global file_name

    config_obj = configparser.ConfigParser()
    config_obj.read("configfile.ini")
    uart_info = config_obj["uart_info"]
    if "port" in uart_info:
        selected_port = uart_info['port']
    if "baud" in uart_info:
        serial_baud_rate = int(uart_info['baud'])
    else:
        serial_baud_rate = 115200
    print(f"[+] Baud rate {serial_baud_rate} has been set")

    json2csv = config_obj["json2csv"]
    json_data = dict()
    if ('info_names' not in json2csv) or ('info_paths' not in json2csv):
        print('[!] Args \'info_names\' and \'info_paths\' are required. Check config file.')
        exit(-1)

    names = eval(json2csv["info_names"])
    paths = eval(json2csv["info_paths"])
    if len(names) != len(paths):
        print("[!] Lens of \'info_names\' and \'info_paths\' are not equal. Check config file.")
        exit(-1)

    for i in range(len(names)):
        json_data[names[i]] = paths[i]

    file_name = '%s/%s.csv' % (folder_output, datetime.now().strftime("%Y-%m-%d %H-%M-%S"))


def select_a_serial_port():
    global selected_port

    available_ports_all = list(serial.tools.list_ports.comports())  # get all available serial ports
    available_ports = [port for port in available_ports_all if port[2] != 'n/a']  # remove all unfit serial ports
    available_ports.sort(key=operator.itemgetter(1))

    if selected_port is not None:
        if [item[0] for item in available_ports].count(selected_port):
            print(f"[+] Port from config file {selected_port}")
            return
        else:
            print(f"[!] Port {selected_port} from config file has not been found")
            selected_port = None

    if len(available_ports) == 0:  # list is empty -> exit
        print("[!] No suitable serial port found.")
        exit(-1)
    elif len(available_ports) == 1:  # only one port available
        (selected_port, _, _) = available_ports[0]
        print("[+] Using only available serial port: %s" % selected_port)
    else:  # let user choose a port
        successful_selection = False
        while not successful_selection:
            print("[+] Select one of the available serial ports:")
            # port selection
            item = 1
            for port, desc, _ in available_ports:
                print("    (%d) %s \"%s\"" % (item, port, desc))
                item += 1
            selected_item = int(input(">>> "))
            # check if a valid item was selected
            if (selected_item > 0) and (selected_item <= len(available_ports)):
                (selected_port, _, _) = available_ports[selected_item - 1]
                successful_selection = True
            else:
                print("[!] Invalid serial port.\n")


def open_selected_serial_port():
    global uart
    try:
        uart = serial.Serial(
            selected_port,
            serial_baud_rate,
            timeout=serial_timeout_read,
            bytesize=serial.EIGHTBITS,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
        )
        print("[+] Successfully connected.")
    except serial.SerialException:
        print("[!] Unable to open %s." % selected_port)
        exit(-1)


def create_csv_file():
    global file_name  # file object for CSV file
    global json_data
    try:
        mkdir(folder_output)  # create the output folder for the CSV files if it does not already exist
    except FileExistsError:
        pass

    with open(file_name, 'w+') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(json_data.keys())
    

def save_info(logs):
    print("-" * 100)
    print(logs)

    info = []
    for key in json_data.keys():
        object_of_key = logs
        for item in json_data[key]:
            try:
                if type(object_of_key) is list:
                    object_of_key = object_of_key[int(item)]
                else:
                    object_of_key = object_of_key[item]
            except KeyError or IndexError:
                print(f'[!] Cant find path for \"{key}\": {json_data[key]}')
                return
        info.append(object_of_key)

    print('[+] Info saved:')
    for i in range(len(json_data)):
        print(f"\t {list(json_data.keys())[i]}: {info[i]}")

    with open(file_name, 'a', newline='\n') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(info)


if __name__ == '__main__':
    get_configfile_info()

    select_a_serial_port()
    create_csv_file()

    open_selected_serial_port()

    print()
    while True:
        line = uart.readline()
        json = re.search(r'{.+}', line.decode("utf-8").strip())
        if json is not None:
            save_info(eval(json.group(0)))
            print()
