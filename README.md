# uart2csv

---

## About The Project
Here's a python script, which takes unstoppable logs from UART, find in them json's and save it in csv fie.


## Installation

1. Clone the repo
   ```sh
   git clone 
   ```
2. Install python libraries
   ```sh
   pip install -r requirements.txt
   ```


## Usage
Before execute the **main.py** you need to set up **configfile.ini**

Config file args with groups:
1) **[uart_info]**
   1) **port = /dev/ttyUSB0** - set uart port (Not required)
   2) **baud = 9600** - baud speed (Default = 115200)
2) **[json2csv]**
   1) **info_names = ["timestamp", "temperature"]** - name of headers (required)
   2) **info_paths = [["measures", 0, "timestamp"], ["measures", 0, "value"]]** - path to value in json (required)

After that you can execute **main.py**
```sh
python3 main.py
```
### Example *configfile.ini*

```
[uart_info]
port = /dev/ttyUSB0
baud = 115200

[json2csv]
info_names = ["timestamp", "temperature"]
info_paths = [["measures", 0, "timestamp"], ["measures", 0, "value"]]
```


    

